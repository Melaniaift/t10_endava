﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;


namespace SeleniumWebdriverWorkshop
{
    static class GlobalVar
    {
        public static ChromeDriver webDriver = new ChromeDriver();
    }
    static class MethodsClass
    {       
        
        public static void FillForm()
        {
            //Open browser
            //IWebDriver webDriver = new ChromeDriver();
            //Navigate to site
            GlobalVar.webDriver.Navigate().GoToUrl("https://demoqa.com/automation-practice-form");
            GlobalVar.webDriver.Manage().Window.Maximize();
            //Identify textbox
            var firstName = GlobalVar.webDriver.FindElement(By.Id("firstName"));
            //Assertion
            Assert.That(firstName.Displayed, Is.True);
            firstName.SendKeys("Melania");

            //Identify textbox
            var lastName = GlobalVar.webDriver.FindElement(By.Id("lastName"));
            //Assertion
            Assert.That(lastName.Displayed, Is.True);
            lastName.SendKeys("I");

            //Identify textbox
            var email = GlobalVar.webDriver.FindElement(By.Id("userEmail"));
            //Assertion
            Assert.That(email.Displayed, Is.True);
            email.SendKeys("me@gmail.com");

            //Identify radiobtn
            GlobalVar.webDriver.FindElement(By.XPath("//label[@for='gender-radio-2']")).Click();
            //Assertion
            Console.WriteLine(GlobalVar.webDriver.FindElement(By.Id("gender-radio-3")).Enabled.Equals(false));

            //Identify textbox
            var date = GlobalVar.webDriver.FindElement(By.Id("userNumber"));
            //Assertion
            Assert.That(date.Displayed, Is.True);
            date.SendKeys("0777777777");

            var subject = GlobalVar.webDriver.FindElement(By.Id("subjectsInput"));
            Assert.That(subject.Displayed, Is.True);
            subject.Click();
            subject.SendKeys("Math");
            subject.SendKeys(Keys.Tab);

            var dateOf = GlobalVar.webDriver.FindElement(By.Id("dateOfBirthInput"));
            Assert.That(dateOf.Displayed, Is.True);
            dateOf.Click();

            for (int i = 0; i <= 9; i++)
            {
                dateOf.SendKeys(Keys.Backspace);
                Thread.Sleep(100);
            }

            dateOf.SendKeys("24 Oct 1999");
            dateOf.SendKeys(Keys.Enter);


            GlobalVar.webDriver.FindElement(By.XPath("//label[@for='hobbies-checkbox-2']")).Click();

            //Identify textbox
            var address = GlobalVar.webDriver.FindElement(By.Id("currentAddress"));
            //Assertion
            Assert.That(address.Displayed, Is.True);
            address.SendKeys("address");

            var state = GlobalVar.webDriver.FindElement(By.Id("state"));
            GlobalVar.webDriver.ExecuteScript("arguments[0].scrollIntoView(true);", state);
            state.Click();
            GlobalVar.webDriver.FindElement(By.XPath("//*[text()='NCR']")).Click();
            Thread.Sleep(1000);
            var city = GlobalVar.webDriver.FindElement(By.Id("city"));
            GlobalVar.webDriver.ExecuteScript("arguments[0].scrollIntoView(true);", state);
            city.Click();
            GlobalVar.webDriver.FindElement(By.XPath("//*[text()='Delhi']")).Click();
            

            var element1 = GlobalVar.webDriver.FindElement(By.XPath("//button[@class ='btn btn-primary']"));
            element1.Click();
            Thread.Sleep(5000);
            // Kill the browser
            GlobalVar.webDriver.Close();

        }
    }
}